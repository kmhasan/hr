package bd.ac.seu.erp.hr.repository;

import bd.ac.seu.erp.hr.model.employeeinformation.Employee;
import bd.ac.seu.erp.hr.model.personalinformation.Name;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void testInsertEmployee() {
        Employee existingEmployee = employeeRepository.findByUsername("kmh");
        if (existingEmployee == null) {
            Employee employee = new Employee();
            employee.setRank("Faculty");
            employee.setUsername("kmh");
            Name name = new Name("Mr.", "Monirul", null, "Hasan", null);
            employee.setName(name);
            Employee savedEmployee = employeeRepository.save(employee);
            assertEquals(employee, savedEmployee);
        }
    }

    @Test
    public void testUpdateExistingEmployee() {
        Employee employee = employeeRepository.findByUsername("kmh");
        employee.setName(new Name("Monirul", "Hasan"));
        Employee savedEmployee = employeeRepository.save(employee);
        assertEquals(employee, savedEmployee);
        assertNull(savedEmployee.getName().getSalutation());
        assertNotEquals(employee.getCreatedAt(), savedEmployee.getLastModifiedAt());
    }
}
