package bd.ac.seu.erp.hr.repository;

import bd.ac.seu.erp.hr.model.personalinformation.*;
import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.Address;
import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.AddressType;
import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.Country;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonRepositoryTest {
    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testInsertPerson() {
        Person person = new Person();
        person.setName(new Name("Mr.", "John", null, "Doe", null));

        long count = personRepository.count();

        Person savedPerson = personRepository.save(person);

        assertEquals(person, savedPerson);
        assertEquals(count + 1, personRepository.count());
    }

    @Test
    public void testAddAddress() {
        Person person = new Person();
        person.setName(new Name("Mr.", "John", null, "Doe", null));

        person.setAddressList(new ArrayList<>());

        Address address1 = new Address(AddressType.WORK_ADDRESS,
                "24 Kemal Ataturk Avenue", "Banani", "Banani", "1213",
                "Dhaka", "Dhaka", "Dhaka", new Country("BD", "Bangladesh"), 90, 21);
        Address address2 = new Address(AddressType.PERMANENT_ADDRESS,
                "24 Kemal Ataturk Avenue", "Banani", "Banani", "1213",
                "Dhaka", "Dhaka", "Dhaka", new Country("BD", "Bangladesh"), 90, 21);

        person.getAddressList().add(address1);
        person.getAddressList().add(address2);

        Person savedPerson = personRepository.save(person);

        assertEquals(2, person.getAddressList().size());
    }

    @Test
    public void testAddFather() {
        Person father = new Person();
        father.setName(new Name("Mr.", "John", null, "Doe", null));
        Person child = new Person();
        child.setName(new Name("Mr.", "John", null, "Doe", "Jr."));

        long count = personRepository.count();

        personRepository.save(father);
        personRepository.save(child);

        assertEquals(count + 2, personRepository.count());

        child.setFather(father);
        father.addChild(child);

        personRepository.save(father);
        personRepository.save(child);

        assertEquals(count + 2, personRepository.count());
        assertEquals(father, child.getFather());
        assertEquals(1, father.getChildList().size());
        assertEquals(father.getChildList().get(0), child);
    }
}
