package bd.ac.seu.erp.hr.model;

import bd.ac.seu.erp.hr.model.employeeinformation.Employee;
import bd.ac.seu.erp.hr.model.employeeinformation.Nominee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ValidationException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeTest {

    @Test(expected = ValidationException.class)
    public void testAddNominee() {
        Employee employee = new Employee();
        Nominee nominee1 = new Nominee(10);
        Nominee nominee2 = new Nominee(91);
        employee.addNominee(nominee1);
        employee.addNominee(nominee2);
    }
}
