package bd.ac.seu.erp.hr.model;

import bd.ac.seu.erp.hr.model.personalinformation.Name;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NameTest {
    @Test
    public void testConstructors() {
        Name name;

        name = new Name(); // no arg constructor
        assertNull(name.getFirstName());
        assertNull(name.getLastName());

        name = new Name("John", "Doe"); // required args constructor
        assertNull(name.getSalutation());
        assertEquals("John", name.getFirstName());
        assertNull(name.getMiddleName());
        assertEquals("Doe", name.getLastName());
        assertNull(name.getSuffix());

        name = new Name("Mr.", "John", "Smith", "Doe", "Sr."); // all args constructor
        assertEquals("Mr.", name.getSalutation());
        assertEquals("John", name.getFirstName());
        assertEquals("Smith", name.getMiddleName());
        assertEquals("Doe", name.getLastName());
        assertEquals("Sr.", name.getSuffix());
    }
}
