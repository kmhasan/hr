package bd.ac.seu.erp.hr.model;

import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.Phone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoneTest {
    @Test
    public void testConstructors() {
        Phone phone = new Phone(Phone.PhoneType.MOBILE, "01811936618");
        assertNull(phone.getCountryCode());
        assertNull(phone.getAreaCode());
        assertNull(phone.getExtension());
        assertEquals(Phone.PhoneType.MOBILE, phone.getPhoneType());
        assertEquals("01811936618",phone.getNumber());
    }

    @Test
    public void testEquality() {
        Phone phone1 = new Phone(Phone.PhoneType.MOBILE, "01811936618");
        Phone phone2 = new Phone(Phone.PhoneType.MOBILE, "01971936618");
        assertNotEquals(phone1, phone2);

        Phone phone3 = new Phone(Phone.PhoneType.MOBILE, "01811936618");
        assertEquals(phone1, phone3);

        Phone phone4 = new Phone(Phone.PhoneType.LAND_PHONE, "7844561");
        phone4.setCountryCode("+88");
        phone4.setAreaCode("02");

        Phone phone5 = new Phone(Phone.PhoneType.LAND_PHONE, "7844561");
        phone5.setCountryCode("+88");
        phone5.setAreaCode("02");
        assertEquals(phone4, phone5);

        phone5.setAreaCode("0421");
        assertNotEquals(phone4, phone5);
    }
}
