package bd.ac.seu.erp.hr.model.personalinformation.contactinformation;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * <h1>Phone Short Description</h1>
 * Briefly describe why you created this file
 *
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 25 Feb 2018 21:39
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"phoneType", "countryCode", "areaCode", "number", "extension"})
public class Phone {
    /**
     * Which Type of phone number it is. Mobile, Phone Fax
     */
    @NonNull @NotNull
    private PhoneType phoneType;    //Mobile

    /**
     * Country Code of the mobile number. Like +88 for Bangladesh
     */
    private String countryCode;     //+88

    /**
     * Area code of the mobile or phone number like 02 for the phone number and 01 for mobile
     */
    private String areaCode;           //for mobile we will leave it blank

    /**
     * Rest of the Phone or Mobile number like 01747942611
     */
    @NonNull @NotNull
    private String number;             //01747942611

    /**
     * Extension
     */
    private String extension; //PABX

    public enum PhoneType {
        LAND_PHONE("Land Phone"),
        MOBILE("Mobile"),
        FAX("Fax");

        /**
         * Type Name of the phone like phone, mobile fax
         */
        private String name;

        PhoneType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
