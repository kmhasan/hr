package bd.ac.seu.erp.hr.model.personalinformation.contactinformation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Country {
    /**
     * Country code of Country like (BD, AU)
     */
    @Id
    private String id;

    /**
     * Full Name of a country
     */
    private String name;
}
