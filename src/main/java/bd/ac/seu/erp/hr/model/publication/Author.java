package bd.ac.seu.erp.hr.model.publication;

import bd.ac.seu.erp.hr.model.personalinformation.Person;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
public class Author extends Person {

    /**
     * Type of the author
     */
    private AuthorType authorType;

    /**
     * From which institute
     */
    private String institute;

    public enum  AuthorType {

        AUTHOR("Author"),
        EDITOR("Editor"),
        TRANSLATOR("Translator");

        /**
         * Type of the author
         */
        private String name;

        AuthorType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
