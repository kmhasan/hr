package bd.ac.seu.erp.hr.repository;

import bd.ac.seu.erp.hr.model.employeeinformation.Employee;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, ObjectId> {
    public Employee findByUsername(String username);
}
