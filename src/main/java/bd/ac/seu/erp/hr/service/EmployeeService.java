package bd.ac.seu.erp.hr.service;

import bd.ac.seu.erp.hr.model.employeeinformation.Employee;
import bd.ac.seu.erp.hr.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void save(Employee employee) {
        employeeRepository.save(employee);
    }
}
