package bd.ac.seu.erp.hr.model.employeeinformation;

import bd.ac.seu.erp.hr.model.personalinformation.Person;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@RequiredArgsConstructor
public class Nominee extends Person {
    @NonNull @NotNull
    private double percentage;
}
