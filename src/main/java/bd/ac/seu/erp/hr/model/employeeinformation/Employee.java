package bd.ac.seu.erp.hr.model.employeeinformation;

import bd.ac.seu.erp.hr.model.personalinformation.Person;
import lombok.*;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Document
public class Employee extends Person implements UserDetails, Persistable<String> {
    @Indexed(unique = true)
    @NonNull @NotNull
    private String username;
    private String rank;

    private List<Nominee> nomineeList;

    @CreatedDate
    private LocalDateTime createdAt;
    @CreatedBy
    private String createdBy;
    @LastModifiedDate
    private LocalDateTime lastModifiedAt;
    @LastModifiedBy
    private String lastModifiedBy;

    public void addNominee(Nominee nominee) {
        if (nomineeList == null)
            nomineeList = new ArrayList<>();
        nomineeList.add(nominee);

        double percentageSum = nomineeList.stream().mapToDouble(Nominee::getPercentage).sum();
        if (percentageSum > 100)
            throw new ValidationException(String.format("Percentage sum %.2f exceeds 100", percentageSum));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isNew() {
        return createdAt == null;
    }
}
