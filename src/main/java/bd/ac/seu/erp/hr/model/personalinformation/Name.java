package bd.ac.seu.erp.hr.model.personalinformation;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Name {
    private String salutation;
    @NonNull
    private String firstName;
    private String middleName;
    @NonNull
    private String lastName;
    private String suffix;
}
