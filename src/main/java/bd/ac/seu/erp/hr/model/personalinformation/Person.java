package bd.ac.seu.erp.hr.model.personalinformation;

import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.Address;
import bd.ac.seu.erp.hr.model.personalinformation.contactinformation.Phone;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"id"})
@RequiredArgsConstructor
@NoArgsConstructor
//@Document
public class Person {
    @Id
    @NonNull
    private String id;
    @NonNull
    private Name name;

    private Person father;
    private Person mother;
    private List<Person> childList;
    private List<Person> spouseList;

    private List<Address> addressList;
    private List<Phone> phoneList;

    private Address primaryAddress;
    private Phone primaryPhone;

    private String occupation;

    public void setFather(Person father) {
        this.father = new Person(father.getId(), father.getName());
    }

    public void setMother(Person mother) {
        this.mother = new Person(mother.getId(), mother.getName());
    }

    public void addChild(Person child) {
        if (childList == null)
            childList = new ArrayList<>();
        childList.add(new Person(child.getId(), child.getName()));
    }

    public void addSpouse(Person spouse) {
        if (spouseList == null)
            spouseList = new ArrayList<>();
        spouseList.add(new Person(spouse.getId(), spouse.getName()));
    }

    public void addAddress(Address address) {
        if (addressList == null)
            addressList = new ArrayList<>();
        addressList.add(address);
    }

    public void addPhone(Phone phone) {
        if (phoneList == null)
            phoneList = new ArrayList<>();
        phoneList.add(phone);
    }

    public void setPrimaryAddress(Address address) {
        this.primaryAddress = address;
        if (addressList == null || !addressList.contains(address))
            addAddress(address);
    }

    public void setPrimaryPhone(Phone phone) {
        this.primaryPhone = phone;
        if (phoneList == null || !phoneList.contains(phone))
            addPhone(phone);
    }
}
