package bd.ac.seu.erp.hr.model.personalinformation.contactinformation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"addressType", "streetAddress", "policeStation", "postOffice", "subDistrict", "district", "division", "country"})
public class Address {
    /**
     * Which type is the address is. Like Present Address/Work Address
     */
    private AddressType addressType;

    /**
     * Floor number, House number, Road number, Section, Area of address
     */
    private String streetAddress;

    /**
     * PoliceStation name depends on the SubDistrict
     */
    private String policeStation;

    /**
     * PostOffice depends on the SubDistrict
     */
    private String postOffice;

    /**
     * PostOffice depends on the SubDistrict
     */
    private String postCode;

    /**
     * SubDistrict name depends
     */
    private String subDistrict;

    /**
     * District name depends
     */
    private String district;

    /**
     * Division name depends on the country
     */
    private String division;


    /**
     * Country of the address
     */
    private Country country;

    /**
     * Address geographic coordinate that specifies the north–south position of a point on the Earth's surface
     */
    private double latitude;

    /**
     * Address geographic coordinate that specifies the east-west position of a point on the Earth's surface.
     */
    private double longitude;
}
