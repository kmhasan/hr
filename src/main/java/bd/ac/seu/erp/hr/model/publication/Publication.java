package bd.ac.seu.erp.hr.model.publication;

import lombok.Data;
import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.net.URL;
import java.util.List;

@Data
//@Document
public class Publication implements Serializable {

    /**
     * Unique Object Id
     */
    @Id
    @Indexed(unique = true)
    @NonNull
    private ObjectId objectId;

    /**
     * What type of publication it is
     */
    private PublicationType publicationType;

    /**
     * Source Title of the Publication
     */
    private String sourceTitle;

    /**
     * Author or Contributor List
     */
    private List<Author> authorList;

    /**
     * Publication Information of the Publication
     */
//    private PublicationInformation publicationInformation;

    /**
     * Is this publication is locally published
     */
    private boolean isLocal;

    /**
     * Percentage of the contribution to do this publication
     */
    private double contributionPercentage;

    /**
     * Publication Website Information of the Publication
     */
    private URL publicationUrl;

    /**
     * Link of DOI
     */
    private URL doi;


    public enum PublicationType {

        JOURNAL("Journal", "A scholarly work published periodically, containing highly specified research."),
        BOOK("Book", "A written work or composition found in print, or digitally as an e-book. Can be non-fiction or fiction."),
        MAGAZINE("Magazine", "A popular work published periodically (weekly, monthly etc.) focusing on a specific interest or subject."),
        NEWSPAPER("News Paper", "A periodical publication containing current events, news, interviews and opinion articles."),
        WEBSITE("Website", "A collection of pages that provides information about a certain topic."),
        FILM("Film", "A motion picture or movie. Can be a fictional movie, documentary or even YouTube videos."),
        ARTICLE("Article", ""),
        CONFERENCE("Conference", ""),
        OTHER("Other", "Thesis/");

        /**
         * Which type of publication it is
         */
        private String name;

        /**
         * Details about the Publication Type
         */
        private String details;

        PublicationType(String name, String details) {
            this.name = name;
            this.details = details;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }
    }


}
