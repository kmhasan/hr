package bd.ac.seu.erp.hr.model.personalinformation.contactinformation;

public enum AddressType {
    PRESENT_ADDRESS("Present Address"),
    PERMANENT_ADDRESS("Permanent Address"),
    WORK_ADDRESS("Work Address");

    /**
     * Type Name of the address like Present Address, Permanent Address, Work Address
     */
    private String name;

    AddressType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
